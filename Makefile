#################################################
#                                               #
#    _____    __                       _____    #
#   /  _  \  |  |    ____  ___  ___   /  |  |   #
#  /  /_\  \ |  |  _/ __ \ \  \/  /  /   |  |_  #
# /    |    \|  |__\  ___/  >    <  /    ^   /  #
# \____|__  /|____/ \___  >/__/\_ \ \____   |   #
#         \/            \/       \/      |__|   #
#                                               #
#   Linux Makefile for Alex the Allegator 4     #
#   adapted from alex4_1.1-6.debian patches     #
#                                               #
#   Author: Pix3lworkshop                       #
#                                               #
#################################################

##
# The data and installation directory
##
PREFIX  = /usr/local
DATADIR = $(PREFIX)/share/$(TARGET)

##
# Compiler and compilation flags
##
CC = gcc
CFLAGS = \
	-O2\
	-fomit-frame-pointer\
	-ffast-math\
	-funroll-loops\
	-W\
	-Wall
LIBS = -laldmb -ldumb `allegro-config --libs`

##
# Sources directory
##
SRC_DIR = src

##
# List of source to compile
##
OBJ =  actor.o    edit.o  map.o       player.o    shooter.o unix.o \
	   bullet.o   hisc.o  options.o   script.o    timer.o          \
	   control.o  main.o  particle.o  scroller.o  token.o

##
# Prefix sources with patch
##
OBJS = $(addprefix $(SRC_DIR)/, $(OBJ))

##
# The executable filename
##
TARGET = alex4

##
# Main action
##
$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

##
# List of source to compile
##
%.o: %.c
	$(CC) $(CPPFLAGS) $(CFLAGS) $(DEFINES) -o $@ -c $<

##
# Install to DATADIR path
##
install: $(TARGET)
	mkdir -p $(PREFIX)/bin
	mkdir -p $(DATADIR)
	install -p -m 755 $(TARGET) $(PREFIX)/bin
	install -p -m 644 ../data/*.dat $(DATADIR)

##
# Clean compiled files
##
clean:
	$(RM) $(OBJS)
	$(RM) $(TARGET)

